import pytest
from selenium import webdriver


@pytest.fixture(scope="function")
def browser():
    browser = webdriver.Chrome()
    yield browser
    browser.quit()

@pytest.fixture(scope="function")
def browser_for_whatsapp():
    # chromepath = r'D:/python_projects/selenium_practic/i2crmtest/chromedriver.exe'
    options = webdriver.ChromeOptions()
    options.add_argument("user-data-dir=C:/Users/LeMaR/AppData/Local/Google/Chrome/User Data")
    browser_for_whatsapp = webdriver.Chrome(options=options)

    yield browser_for_whatsapp
    browser_for_whatsapp.quit()