from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


class InstdirCreateLocators():
    INSTDIR_CREATE = (By.LINK_TEXT, "Instagram::Директы")
    CREATE_FORM_USERNAME = (By.ID, "instdir-username")
    CREATE_FORM_PASSWORD = (By.ID, "instdir-new_password")
    CREATE_INSTDIR_GO = (By.CLASS_NAME, "btn-success")


class InstagramLoginLocators():
    INSTAGRAM_USERNAME = (By.CSS_SELECTOR, "input[name='username']")
    INSTAGRAM_PASSWORD = (By.CSS_SELECTOR, "input[name='password']")
    # INSTAGRAM_USERNAME = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input[name = 'username']")))
    # INSTAGRAM_PASSWORD = WebDriverWait(driver, 10).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "input [name = 'password']")))
    INSTAGRAM_LOGIN = (By.CSS_SELECTOR, "button[type='submit']")
    #LABEL_DIRECT = (By.CLASS_NAME, "xWeGp")
    LABEL_DIRECT = (By.XPATH, '//*[@id="react-root"]/section/nav/div[2]/div/div/div[3]/div/div[2]/a')
    BUTTON_DIRECT = (By.XPATH, '//*[@id="react-root"]/section/div/div[2]/div/div/div[2]/div/div[3]/div/button')
    POPUP_1 = (By.XPATH, '/html/body/div[5]/div/div/div/div[3]/button[2]')
    DIRECT_SEARCH = (By.CSS_SELECTOR, "input[name='queryBox']")
    RADIO_DIRECT = (By.CLASS_NAME, "dCJp8")
    FURTHER_DIRECT = (By.CLASS_NAME, "rIacr")

class InstagramSendLocators():
    INPUT_DIRECT = (By.XPATH, '//*[@id="react-root"]/section/div/div[2]/div/div/div[2]/div[2]/div/div[2]/div/div/div[2]/textarea')
    SEND_DIRECT = (By.XPATH, '//*[@id="react-root"]/section/div/div[2]/div/div/div[2]/div[2]/div/div[2]/div/div/div[3]/button')
