from selenium.webdriver.common.by import By


class LoginPageLocators():
    LOGIN_FORM_EMAIL = (By.ID, "loginform-email")
    LOGIN_FORM_PASSWORD = (By.ID, "loginform-password")
    LOGIN_GO = (By.NAME, "login-button")
    LOGIN_SUCCESS = (By.CLASS_NAME, "profile-plan")


class SignupPageLocators():
    SIGNUP_FORM_NAME = (By.ID, "signup-name")
    SIGNUP_FORM_EMAIL = (By.ID, "signup-email")
    SIGNUP_FORM_PHONE = (By.ID, "signup-phone_national")
    SIGNUP_GO = (By.NAME, "signup-button")
    SIGNUP_SUCCESS = (By.CLASS_NAME, "site-login")


class SettingsPageLocators():
    SETTINGS_PAGE = (By.LINK_TEXT, "Настройки")


class InChannelsPageLocators():
    IN_CHANNELS = (By.LINK_TEXT, "Входящие каналы")


class InChannelsCreatePageLocators():
    IN_CHANNELS_CREATE = (By.XPATH, '//*[@id="sourcesForm"]/div[1]/div[1]/div/button')