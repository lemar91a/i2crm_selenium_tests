from selenium.webdriver.common.by import By


class WhatsappCreateLocators():
    WHATSAPP_CREATE = (By.LINK_TEXT, "WhatsApp")
    WHATSAPP_BUTTON_CLICK = (By.ID, "openTokenPopup")
    WHATSAPP_GO = (By.CLASS_NAME, "btn-success")