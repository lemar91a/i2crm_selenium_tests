from selenium.webdriver.common.by import By


class InstcomOldCreateLocators():
    INSTCOM_OLD_CREATE = (By.LINK_TEXT, "Instagram::Комментарии")
    CREATE_FORM_USERNAME = (By.ID, "instcom-username")
    CREATE_INSTCOM_GO = (By.CLASS_NAME, "btn-success")