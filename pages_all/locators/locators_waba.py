from selenium.webdriver.common.by import By


class WabaCreateLocators():
    WABA_CREATE = (By.LINK_TEXT, "WhatsApp Business")
    WABA_FORM_USERNAME = (By.ID, "waba-username")
    WABA_FORM_API = (By.ID, "waba-new_token")
    CREATE_WABA_GO = (By.CLASS_NAME, "btn-success")

