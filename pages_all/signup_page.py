from .base_page import BasePage
from pages_all.locators.locators import SignupPageLocators
from .data_all.data import SIGNUP_FORM_NAME, SIGNUP_FORM_EMAIL, SIGNUP_FORM_PHONE


class SignupPage(BasePage):
    def signup(self):
        name_input = self.browser.find_element(*SignupPageLocators.SIGNUP_FORM_NAME)
        name_input.clear()
        name_input.send_keys(SIGNUP_FORM_NAME)
        email_input = self.browser.find_element(*SignupPageLocators.SIGNUP_FORM_EMAIL)
        email_input.clear()
        email_input.send_keys(SIGNUP_FORM_EMAIL)
        phone_input = self.browser.find_element(*SignupPageLocators.SIGNUP_FORM_PHONE)
        phone_input.clear()
        phone_input.send_keys(SIGNUP_FORM_PHONE)

        login_go = self.browser.find_element(*SignupPageLocators.SIGNUP_GO)
        login_go.click()
        self.browser.implicitly_wait(10)

        assert self.is_element_present(*SignupPageLocators.SIGNUP_SUCCESS), "Profile plan is not presented"


