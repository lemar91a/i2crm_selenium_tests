from pages_all.base_page import BasePage
from pages_all.locators.locators_instdir import InstagramLoginLocators, InstagramSendLocators
from pages_all.data_all.data import INST_USERNAME_SEND, INST_PASSWORD_SEND
from pages_all.data_all.data_send_instdir import SMALL_TEXT
import time

class InstagramPage(BasePage):
    def login(self):
        time.sleep(5)
        login_input = self.browser.find_element(*InstagramLoginLocators.INSTAGRAM_USERNAME)
        login_input.clear()
        login_input.send_keys(INST_USERNAME_SEND)
        password_input = self.browser.find_element(*InstagramLoginLocators.INSTAGRAM_PASSWORD)
        password_input.clear()
        password_input.send_keys(INST_PASSWORD_SEND)
        login_go = self.browser.find_element(*InstagramLoginLocators.INSTAGRAM_LOGIN)
        login_go.click()
        # time.sleep(200)
        time.sleep(5)

    def direct(self):
        label_direct = self.browser.find_element(*InstagramLoginLocators.LABEL_DIRECT)
        label_direct.click()
        time.sleep(3)

        popup = self.browser.find_element(*InstagramLoginLocators.POPUP_1)
        popup.click()

        button_direct = self.browser.find_element(*InstagramLoginLocators.BUTTON_DIRECT)
        button_direct.click()

        direct = self.browser.find_element(*InstagramLoginLocators.DIRECT_SEARCH)
        direct.clear()
        direct.send_keys(INST_USERNAME_SEND)
        time.sleep(2)
        button_direct = self.browser.find_element(*InstagramLoginLocators.RADIO_DIRECT)
        button_direct.click()
        further_direct = self.browser.find_element(*InstagramLoginLocators.FURTHER_DIRECT)
        further_direct.click()

        time.sleep(2)

    def send_message_text(self):
        send_text = self.browser.find_element(*InstagramSendLocators.INPUT_DIRECT)
        send_text.clear()
        send_text.send_keys(SMALL_TEXT)
        send_text = self.browser.find_element(*InstagramSendLocators.SEND_DIRECT)
        send_text.click()
        time.sleep(20)