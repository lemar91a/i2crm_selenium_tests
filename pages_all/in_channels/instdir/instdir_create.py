from pages_all.base_page import BasePage
from pages_all.locators.locators_instdir import InstdirCreateLocators
from pages_all.locators.locators import SettingsPageLocators, InChannelsPageLocators, InChannelsCreatePageLocators
from pages_all.data_all.data import LOGIN_INST_USERNAME, LOGIN_INST_PASSWORD
import time

class InstdirCreatePage(BasePage):
    def create(self):
        sett = self.browser.find_element(*SettingsPageLocators.SETTINGS_PAGE)
        sett.click()
        sett = self.browser.find_element(*InChannelsPageLocators.IN_CHANNELS)
        sett.click()
        sett = self.browser.find_element(*InChannelsCreatePageLocators.IN_CHANNELS_CREATE)
        sett.click()
        sett = self.browser.find_element(*InstdirCreateLocators.INSTDIR_CREATE)
        sett.click()

        login_input = self.browser.find_element(*InstdirCreateLocators.CREATE_FORM_USERNAME)
        login_input.clear()
        login_input.send_keys(LOGIN_INST_USERNAME)
        password_input = self.browser.find_element(*InstdirCreateLocators.CREATE_FORM_PASSWORD)
        password_input.clear()
        password_input.send_keys(LOGIN_INST_PASSWORD)
        login_go = self.browser.find_element(*InstdirCreateLocators.CREATE_INSTDIR_GO)
        login_go.click()
        time.sleep(20)