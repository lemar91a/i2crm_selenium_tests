from pages_all.base_page import BasePage
from pages_all.locators.locators_waba import WabaCreateLocators
from pages_all.locators.locators import SettingsPageLocators, InChannelsPageLocators, InChannelsCreatePageLocators
from pages_all.data_all.data import NAME, API
import time

class WabaCreatePage(BasePage):
    def create(self):
        sett = self.browser.find_element(*SettingsPageLocators.SETTINGS_PAGE)
        sett.click()
        sett = self.browser.find_element(*InChannelsPageLocators.IN_CHANNELS)
        sett.click()
        sett = self.browser.find_element(*InChannelsCreatePageLocators.IN_CHANNELS_CREATE)
        sett.click()
        sett = self.browser.find_element(*WabaCreateLocators.WABA_CREATE)
        sett.click()

        login_input = self.browser.find_element(*WabaCreateLocators.WABA_FORM_USERNAME)
        login_input.clear()
        login_input.send_keys(NAME)
        password_input = self.browser.find_element(*WabaCreateLocators.WABA_FORM_API)
        password_input.clear()
        password_input.send_keys(API)
        login_go = self.browser.find_element(*WabaCreateLocators.CREATE_WABA_GO)
        login_go.click()
        time.sleep(200)