from pages_all.base_page import BasePage
from pages_all.locators.locators_instcom_old_create import InstcomOldCreateLocators
from pages_all.locators.locators import SettingsPageLocators, InChannelsPageLocators, InChannelsCreatePageLocators
from pages_all.data_all.data import LOGIN_INST_USERNAME
import time

class InstcomOldCreatePage(BasePage):
    def create(self):
        sett = self.browser.find_element(*SettingsPageLocators.SETTINGS_PAGE)
        sett.click()
        sett = self.browser.find_element(*InChannelsPageLocators.IN_CHANNELS)
        sett.click()
        sett = self.browser.find_element(*InChannelsCreatePageLocators.IN_CHANNELS_CREATE)
        sett.click()
        sett = self.browser.find_element(*InstcomOldCreateLocators.INSTCOM_OLD_CREATE)
        sett.click()

        username_input = self.browser.find_element(*InstcomOldCreateLocators.CREATE_FORM_USERNAME)
        username_input.clear()
        username_input.send_keys(LOGIN_INST_USERNAME)
        create_go = self.browser.find_element(*InstcomOldCreateLocators.CREATE_INSTCOM_GO)
        create_go.click()

        time.sleep(40)