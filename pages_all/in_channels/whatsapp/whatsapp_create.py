from pages_all.base_page import BasePage
from pages_all.locators.locators_whatsapp import WhatsappCreateLocators
from pages_all.locators.locators import SettingsPageLocators, InChannelsPageLocators, InChannelsCreatePageLocators
import time

class WhatsappCreatePage(BasePage):
    def create(self):
        sett = self.browser.find_element(*SettingsPageLocators.SETTINGS_PAGE)
        sett.click()
        sett = self.browser.find_element(*InChannelsPageLocators.IN_CHANNELS)
        sett.click()
        sett = self.browser.find_element(*InChannelsCreatePageLocators.IN_CHANNELS_CREATE)
        sett.click()
        sett = self.browser.find_element(*WhatsappCreateLocators.WHATSAPP_CREATE)
        sett.click()

        whatsapp_create = self.browser.find_element(*WhatsappCreateLocators.WHATSAPP_BUTTON_CLICK)
        whatsapp_create.click()
        time.sleep(10)
        whatsapp_create = self.browser.find_element(*WhatsappCreateLocators.WHATSAPP_GO)
        whatsapp_create.click()
        time.sleep(10)