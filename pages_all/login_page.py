from .base_page import BasePage
from pages_all.locators.locators import LoginPageLocators
from pages_all.data_all.data import EMAIL_LOGIN, PASSWORD_LOGIN


class LoginPage(BasePage):
    def login(self):
        email_input = self.browser.find_element(*LoginPageLocators.LOGIN_FORM_EMAIL)
        email_input.clear()
        email_input.send_keys(EMAIL_LOGIN)
        password_input = self.browser.find_element(*LoginPageLocators.LOGIN_FORM_PASSWORD)
        password_input.clear()
        password_input.send_keys(PASSWORD_LOGIN)
        login_go = self.browser.find_element(*LoginPageLocators.LOGIN_GO)
        login_go.click()
        self.browser.implicitly_wait(10)

        assert self.is_element_present(*LoginPageLocators.LOGIN_SUCCESS), "Profile plan is not presented"


