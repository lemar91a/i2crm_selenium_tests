from pages_all.in_channels.instcom.instcom_old_create import InstcomOldCreatePage
from pages_all.login_page import LoginPage


def test_instcom_old_create(browser):
    # способ залогиниться нужно отрефакторить, чтобы было по DRY
    link = "https://app.i2crm.ru/manage/site/login"
    page = LoginPage(browser, link)  # инициализируем Page Object, передаем в конструктор экземпляр драйвера и url адрес
    page.open()
    page.login()
    instcom_old = InstcomOldCreatePage(browser,link)
    instcom_old.create()