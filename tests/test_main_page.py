from pages_all.login_page import LoginPage
from pages_all.signup_page import SignupPage


def test_login(browser):
    link = "https://app.i2crm.ru/manage/site/login"
    page = LoginPage(browser, link) # инициализируем Page Object, передаем в конструктор экземпляр драйвера и url адрес
    page.open()
    page.login()

def test_signup(browser):
    link = "https://app.i2crm.ru/manage/site/signup"
    page = SignupPage(browser, link) # инициализируем Page Object, передаем в конструктор экземпляр драйвера и url адрес
    page.open()
    page.signup()
