from pages_all.in_channels.instdir.instdir_create import InstdirCreatePage
from pages_all.login_page import LoginPage


def test_instdir_create(browser):
    # способ залогиниться нужно отрефакторить, чтобы было по DRY
    link = "https://app.i2crm.ru/manage/site/login"
    page = LoginPage(browser, link)  # инициализируем Page Object, передаем в конструктор экземпляр драйвера и url адрес
    page.open()
    page.login()
    instdir = InstdirCreatePage(browser,link)
    instdir.create()



