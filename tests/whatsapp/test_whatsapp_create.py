from pages_all.in_channels.whatsapp.whatsapp_create import WhatsappCreatePage
from pages_all.login_page import LoginPage


def test_whatsapp_create(browser_for_whatsapp):
    # способ залогиниться нужно отрефакторить, чтобы было по DRY
    link = "https://app.i2crm.ru/manage/site/login"
    # page = LoginPage(browser_for_whatsapp, link)  # инициализируем Page Object, передаем в конструктор экземпляр драйвера и url адрес
    # page.open()
    # page.login()
    whatsap = WhatsappCreatePage(browser_for_whatsapp,link)
    whatsap.open()
    whatsap.create()