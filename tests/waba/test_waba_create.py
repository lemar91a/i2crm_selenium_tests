from pages_all.in_channels.waba.waba_create import WabaCreatePage
from pages_all.login_page import LoginPage


def test_waba_create(browser):
    # способ залогиниться нужно отрефакторить, чтобы было по DRY
    link = "https://app.i2crm.ru/manage/site/login"
    page = LoginPage(browser, link)  # инициализируем Page Object, передаем в конструктор экземпляр драйвера и url адрес
    page.open()
    page.login()
    waba = WabaCreatePage(browser,link)
    waba.create()